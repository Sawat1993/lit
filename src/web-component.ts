import { LitElement, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { Chart } from './chart';

@customElement('program-chart')
class WebElement extends LitElement {
    chart = new Chart()
    @property({ type: String })
    chartid = '';
    @property({ type: String })
    data = '[]';

    attributeChangedCallback(name: string, _old: string | null, value: string | null): void {
        if(name === 'chartid') {
            console.log('chartid', value)
            this.chart.createProgramChart(value ? value : undefined)
        }
        if(name === 'data') {
            console.log('data', value)
            this.chart.setData(value ? JSON.parse(value) : [])
        }
    }

    connectedCallback() {
        super.connectedCallback()
        console.log('mounted')
    }

    render() {
        return html`
      <div>This is Program Chart!</div>
      <slot></slot>
    `;
    }
}
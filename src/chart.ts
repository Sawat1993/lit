import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";

export class Chart {
    chart: any;
    createProgramChart(id = 'chartdiv') {
        let chart = am4core.create(id, am4charts.PieChart);

        // Create pie series
        let series = chart.series.push(new am4charts.PieSeries());
        series.dataFields.value = "litres";
        series.dataFields.category = "country";
        this.chart = chart

        this.setData([]);
    }

    setData(data: any) {
        if(this.chart) {
            this.chart.data = data;
        }
    }
}